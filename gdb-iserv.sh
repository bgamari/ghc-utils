#!/usr/bin/env bash

set -eux

GHC="$(command -v $1)"
WRAPPER="${WRAPPER:-$(command -v rr)}"

usage() {
    echo "usage: $0 GHC ARGS..."
    echo ""
    echo "Invokes GHC with the given arguments, wrapping ghc-iserv"
    echo "executables with the wrapper given by the WRAPPER environment"
    echo "variable (defaulting to rr)."
    exit 1
}

if test -z "$GHC"; then
    usage
fi
if test ! -e "$WRAPPER"; then
    echo "WRAPPER not an executable."
    echo ""
    usage
fi

shift
args=( $@ )

# We create a wrapper for each ghc-iserv-* executable, each invoking the
# appropriate GHC executable through rr.
wrapper_dir="$(mktemp -d ghc-iserv.XXXX)"

mk_wrapper() {
    local suffix="$1"
    local out="$wrapper_dir/ghc-iserv$suffix"
    cat >"$out" <<-EOF
#!/bin/sh
exec "$WRAPPER" record "$GHC-iserv$suffix" \$@
EOF
    chmod ugo+rx "$out"
}

mk_wrapper ""
mk_wrapper "-dyn"
mk_wrapper "-prof"

# Invoke GHC with the wrapped iserv.
err=0
"$GHC" -pgmi "$(command -v "./$wrapper_dir/ghc-iserv")" ${args[@]} || err=$?
rm -Rf "$wrapper_dir"
exit $err
