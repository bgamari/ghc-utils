{-# LANGUAGE RecordWildCards #-}

module NonMovingCensus
    ( CensusPoint(..)
    , eventlogCensusPoints
    , occupancy
    , blocksPerSegment
    ) where

import Data.Word
import Data.Maybe

import GHC.RTS.Events hiding (time)

data CensusPoint = CensusPoint { time         :: !Timestamp
                               , logBlockSize :: !Word8
                               , activeSegs   :: !Word32
                               , filledSegs   :: !Word32
                               , liveBlocks   :: !Word32
                               }

eventlogCensusPoints :: EventLog -> [CensusPoint]
eventlogCensusPoints elog =
    toCensusPoints $ sortEvents $ events $ dat elog

toCensusPoints :: [Event] -> [CensusPoint]
toCensusPoints = mapMaybe f
  where
    f (Event t NonmovingHeapCensus{..} _) =
      Just (CensusPoint { time         = t
                        , logBlockSize = nonmovingCensusBlkSize
                        , activeSegs   = nonmovingCensusActiveSegs
                        , filledSegs   = nonmovingCensusFilledSegs
                        , liveBlocks   = nonmovingCensusLiveBlocks
                        })
    f _ = Nothing


occupancy :: CensusPoint -> Double
occupancy cp
  | allocdBlocks == 0 = 1
  | otherwise = realToFrac (liveBlocks cp) / realToFrac allocdBlocks
  where
    allocdBlocks  = (filledSegs cp + activeSegs cp) * blocksPerSegment (logBlockSize cp)

blocksPerSegment :: Word8 -> Word32
blocksPerSegment logBlkSize =
    fromIntegral (segmentSize - headerSize) `div` (2^logBlkSize)
  where
    headerSize = 3 * 8 -- bytes
    segmentSize = 2^15 -- bytes

