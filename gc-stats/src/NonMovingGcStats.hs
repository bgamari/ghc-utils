{-# LANGUAGE DeriveGeneric #-}

module NonMovingGcStats where

import GHC.Generics
import Data.Aeson (ToJSON(..), ToJSONKey)
import Data.Monoid
import qualified Data.Set as S
import Data.Maybe
import Data.Word
import GHC.RTS.Events

import Types

data NonMovingGc = NonMovingGc { markPhases :: S.Set (Interval, Int)
                               , syncPhase :: Interval
                               , sweepPhase :: Interval
                               , updRemSetFlushes :: S.Set (Timestamp, Cap)
                               }
                 deriving (Show, Generic)
instance ToJSON NonMovingGc

readNonMovingGcs :: FilePath -> IO [NonMovingGc]
readNonMovingGcs inFile = do
    Right elog <- readEventLogFromFile inFile
    return $ toNonMovingGcs $ sortEvents $ events $ dat elog

toNonMovingGcs :: [Event] -> [NonMovingGc]
toNonMovingGcs = goStartMark s0
  where
    s0 = NonMovingGc mempty undefined undefined mempty

    goStartMark :: NonMovingGc -> [Event] -> [NonMovingGc]
    goStartMark s (Event t ConcMarkBegin _ : xs) = goEndMark s t xs
    goStartMark s (Event t ConcSyncBegin _ : xs) =
      let s' = s { syncPhase = Interval t undefined }
      in goStartMark s' xs
    goStartMark s (Event t ConcSyncEnd _ : xs) =
      let s' = s { syncPhase = Interval (intStart $ syncPhase s) t }
      in goStartSweep s' xs
    goStartMark s (Event t (ConcUpdRemSetFlush n) _ : xs) =
      let s' = s { updRemSetFlushes = S.insert (t, Cap n) (updRemSetFlushes s) }
      in goStartMark s' xs
    goStartMark s (_ : xs) = goStartMark s xs
    goStartMark s [] = []

    goEndMark :: NonMovingGc -> Timestamp -> [Event] -> [NonMovingGc]
    goEndMark s t0 (Event t (ConcMarkEnd n) _ : xs) =
      let s' = s { markPhases = S.insert (Interval t0 t, fromIntegral n) (markPhases s) }
      in goStartMark s' xs
    goEndMark s t0 (Event t (ConcUpdRemSetFlush n) _ : xs) =
      let s' = s { updRemSetFlushes = S.insert (t, Cap n) (updRemSetFlushes s) }
      in goEndMark s' t0 xs
    goEndMark s t0 (_ : xs) = goEndMark s t0 xs
    goEndMark s _ [] = []

    goStartSweep :: NonMovingGc -> [Event] -> [NonMovingGc]
    goStartSweep s (Event t ConcSweepBegin _ : xs) = goEndSweep s t xs
    goStartSweep s (_ : xs) = goStartSweep s xs
    goStartSweep s [] = []

    goEndSweep :: NonMovingGc -> Timestamp -> [Event] -> [NonMovingGc]
    goEndSweep s t0 (Event t ConcSweepEnd _ : xs) =
      let s' = s { sweepPhase = Interval t0 t }
      in s' : goStartMark s0 xs
    goEndSweep s t0 (_ : xs) = goEndSweep s t0 xs
    goEndSweep s _ [] = []

