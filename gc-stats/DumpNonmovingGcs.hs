import Data.Aeson (encode)
import Data.List (intercalate)
import qualified Data.ByteString.Lazy as BSL
import System.Environment
import GcStats
import NonMovingGcStats
import Types

main :: IO ()
main = do
    inFile : _ <- getArgs
    gcs <- readNonMovingGcs inFile
    BSL.writeFile (inFile<>".nonmoving-gcs.json") $ encode gcs
