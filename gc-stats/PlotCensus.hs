{-# LANGUAGE RecordWildCards #-}

import System.Environment
import Data.List (intercalate)
import Data.Maybe
import Data.Word
import Data.Default
import Data.Colour
import Data.Colour.Names

import Control.Lens
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Grid
import Graphics.Rendering.Chart.Backend.Cairo
import GHC.RTS.Events hiding (blockSize, time)
import qualified GHC.RTS.Events as E

import NonMovingCensus

plotCensus :: Bool -> [CensusPoint] -> Renderable (LayoutPick Double Double Double)
plotCensus normalize points = fillBackground (fill_color .~ opaque white $ def) (gridToRenderable grid)
  where
    sizes = [4..8]

    grid =
      aboveN 
      $ [ layoutLRToGrid $ layoutSize blkSize 
        | blkSize <- sizes
        ]
     ++ [ layoutToGrid $ layoutTotal ]

    colors = map opaque [ blue, red, green, grey, orange, yellow ]

    layoutTotal =
          layout_plots .~ zipWith f sizes colors
        $ def
      where
        f blkSize color = 
            toPlot
            $ plot_points_title .~ ("block size " <> show (2^blkSize))
            $ plot_points_style . point_color .~ color
            $ plot_points_values .~ 
                [ (realToFrac (time p) / 1e-9, bytes)
                | p <- points
                , logBlockSize p == blkSize
                , let bytes = realToFrac (liveBlocks p) * realToFrac (logBlockSize p)
                ]
            $ def

    layoutSize :: Word8 -> LayoutLR Double Double Double
    layoutSize blkSize =
          layoutlr_title .~ ("Block size = " <> show (2^blkSize))
        $ layoutlr_left_axis . laxis_title .~ "number of blocks"
        $ layoutlr_right_axis . laxis_title .~ right_label
        $ layoutlr_plots .~ plotSize blkSize
        $ def
      where
        right_label
          | normalize =  "percent occupancy"
          | otherwise =  "number of blocks"

    plotSize :: Word8 -> [Either (Plot Double Double) (Plot Double Double)]
    plotSize blkSize =
        [ Left $ toPlot
          $ plot_points_title .~ "active segments"
          $ plot_points_style . point_color .~ opaque green
          $ plotValue (realToFrac . activeSegs)

        , Left $ toPlot
          $ plot_points_title .~ "filled segments"
          $ plot_points_style . point_color .~ opaque red
          $ plotValue (realToFrac . filledSegs)

        , Right $ if normalize then occupancyPlot else liveBlocksPlot
        ]
      where
        liveBlocksPlot = 
          toPlot
          $ plot_points_title .~ "live blocks"
          $ plot_points_style . point_color .~ opaque blue
          $ plotValue (realToFrac . liveBlocks)

        occupancyPlot =
          toPlot
          $ plot_points_title .~ "occupancy"
          $ plot_points_style . point_color .~ opaque blue
          $ plotValue occupancy

        plotValue :: (CensusPoint -> Double) -> PlotPoints Double Double
        plotValue f = 
              plot_points_values .~ [ (realToFrac (time p) / 1e-9, f p)
                                    | p <- points
                                    , logBlockSize p == blkSize
                                    ]
            $ def

main :: IO ()
main = do
    inFile : _ <- getArgs
    Right elog <- readEventLogFromFile inFile
    let points = eventlogCensusPoints elog
    let r = 1000
        opts = fo_format .~ SVG 
             $ fo_size .~ (r, r*2)
             $ def
    renderableToFile opts "census.svg" $ toRenderable $ plotCensus True points

    let pointRow cp@CensusPoint{..} =
            [ show time
            , show (2^logBlockSize)
            , show activeSegs
            , show filledSegs
            , show liveBlocks
            , show (occupancy cp)
            ]
    writeFile "census.tsv" $ unlines $ map (intercalate "\t" . pointRow) points
