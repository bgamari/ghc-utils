let sources = import ../nix/sources.nix; in

{ pkgs ? (import sources.nixpkgs {}) }:

with pkgs; rec {
  debug-ghc = runCommand "debug-ghc" {
  } ''
    mkdir -p $out/bin
    cp ${../debug-ghc} $out/bin/debug-ghc
    chmod ugo+rx $out/bin/debug-ghc
    substituteInPlace $out/bin/debug-ghc \
      --replace tempfile ${debianutils}/bin/tempfile \
      --replace 'PROG="gdb' 'PROG="${gdb}/bin/gdb'
  '';

  parallel-rr = runCommand "parallel-rr" {
  } ''
    mkdir -p $out/bin
    makeWrapper ${../parallel-rr.py} $out/bin/parallel-rr \
      --prefix PATH : ${rr}/bin
  '';

  ghc-gdb = python3Packages.buildPythonPackage {
    name = "ghc-gdb";
    src = ./.;
    preferLocalBuild = true;
    checkInputs = [ mypy ];
    checkPhase = ''
      ${pkgs.mypy}/bin/mypy --ignore-missing-imports --exclude=build .
    '';
  };

  run-ghc-gdb = writeScriptBin "ghc-gdb" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    ${gdb}/bin/gdb -x ${gdbinit}/gdbinit "$@"
  '';

  run-ghc-rr = writeScriptBin "ghc-rr" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    args="$@"
    if [[ "$1" == "replay" ]]; then
      args="$args --debugger ${gdb}/bin/gdb -x ${gdbinit}/gdbinit"
    fi
    ${rr}/bin/rr $args
  '';

  libipt = stdenv.mkDerivation {
    name = "libipt";
    nativeBuildInputs = [ cmake ];
    src = sources.processor-trace;
  };

  gdb-walkers = sources.gdb-walkers;

  gdb = (pkgs.gdb.override {
    python3 = python3;
  }).overrideAttrs (oldAttrs: {
    buildInputs = oldAttrs.buildInputs ++ [ libipt ];
  });

  rr = pkgs.rr.overrideAttrs (oldAttrs: rec {
    patches = [ ];
    src = sources.rr;
  });

  zen-workaround = runCommand "zen-workaround"
    { nativeBuildInputs = [ pkgs.python3 pkgs.makeWrapper ]; }
    ''
      mkdir -p $out/bin
      makeWrapper ${rr.src}/scripts/zen_workaround.py $out/bin/zen_workaround.py
    '';

  pythonEnv = python3.withPackages (_: [ ghc-gdb ]);

  env = symlinkJoin {
    name = "gdb-with-ghc-gdb";
    paths = [
      gdb pythonEnv gdbinit rr dot2svg
      run-ghc-gdb run-ghc-rr zen-workaround
    ];
  };

  # useful to render `ghc closure-deps` output
  dot2svg = writeScriptBin "dot2svg" ''
    #!${pkgs.bash}/bin/bash
    set -eu -o pipefail

    if [[ $# == 0 ]]; then
      echo "Usage: $0 [dot file]"
      exit 1
    fi
    ${graphviz}/bin/dot -T svg -o $1.svg $1
  '';

  gdbinit = writeTextFile {
    name = "gdbinit";
    destination = "/gdbinit";
    text = let
      ghc-gdb-init = ''
        python sys.path = ["${pythonEnv}/lib/python${python3.pythonVersion}/site-packages"] + sys.path
        python
        if 'ghc_gdb' in globals():
            import importlib
            importlib.reload(ghc_gdb)
        else:
            try:
                import ghc_gdb
            except Exception as e:
                import textwrap
                print('Failed to load ghc_gdb:')
                print('  ', e)
                print("")
                print(textwrap.dedent("""
                  If the failure is due to a missing symbol or type try
                  running `import ghc_gdb` after running the inferior.
                  This will load debug information that is lazily
                  loaded.
                """))
        end

        echo The `ghc` command is now available.\n
      '';

      gdb-walkers-init = ''
        python sys.path = ["${gdb-walkers}"] + sys.path
        source ${gdb-walkers}/basic_config.py
        source ${gdb-walkers}/commands.py
        source ${gdb-walkers}/functions.py
      '';
    in lib.concatStringsSep "\n" [ ghc-gdb-init gdb-walkers-init ];
  };
}

