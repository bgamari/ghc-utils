import gdb
import math
from .types import Ptr
from typing import Dict, NamedTuple, Iterator, List, Tuple

class Symbol(NamedTuple):
    obj_path: str
    name: str

SymbolMap = Dict[Ptr, Symbol]

def oc_symbol_map(oc) -> SymbolMap:
    n = int(oc['n_symbols'])
    accum = {}
    obj_path = oc['file_name']
    for i in range(n):
        addr = Ptr(int(oc['symbols'][i]['addr']))
        sym = str(oc['symbols'][i]['name'])
        accum[addr] = Symbol(obj_path=obj_path, name=sym)

    return accum

def all_loaded_objects() -> Iterator[gdb.Value]:
    x = gdb.parse_and_eval('objects')
    while True:
        yield x
        if x['next'].address == 0:
            return
        else:
            x = x['next']

def symbol_map() -> SymbolMap:
    accum = {}
    for oc in all_loaded_objects():
        accum.update(oc_symbol_map(oc))

    return accum

def nearest_symbols(smap: SymbolMap, addr: Ptr) -> List[Tuple[Ptr, Symbol]]:
    return list(sorted(smap.items(), key=lambda x: abs(x[0].addr() - addr.addr())))

#print(nearest_symbols(gdb.parse_and_eval('oc'), 0x500c8140)[:10])

