#!/usr/bin/env python3

from copy import copy
import subprocess
import os
import json
from pathlib import Path

def read_json(fp: Path) -> object:
    s = fp.read_text()
    s = ''.join(l for l in s.split('\n') if not l.startswith('#'))
    return json.loads(s)


def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-j', '--jobs', type=Path, default='./.gitlab/jobs.yaml', metavar='JOBS.YAML',
                        help='path to jobs.yaml')
    subparsers = parser.add_subparsers()

    subp = subparsers.add_parser('list')
    subp.set_defaults(mode='list')

    subp = subparsers.add_parser('dump')
    subp.set_defaults(mode='dump')
    subp.add_argument('job', metavar='JOB NAME',
                      help='the name of the GitLab job to run')

    subp = subparsers.add_parser('run')
    subp.set_defaults(mode='run')
    subp.add_argument('job', metavar='JOB NAME',
                      help='the name of the GitLab job to run')
    subp.add_argument('command', metavar='COMMAND', nargs='*',
                      help='run the given command')

    args = parser.parse_args()
    jobs = read_json(args.jobs)

    if args.mode == 'list':
        print('\n'.join(jobs.keys()))
        return

    job = jobs[args.job]

    def mk_script(attr: str) -> str:
        if attr in job:
            return ' && '.join(job[attr])
        else:
            return 'true'

    env = copy(job['variables'])
    env['SCRIPT'] = mk_script('script')
    env['AFTER_SCRIPT'] = mk_script('after_script')
    env['BEFORE_SCRIPT'] = mk_script('before_script')

    if args.mode == 'dump':
        print('\n'.join(f'{k}="{v}"' for k,v in env.items()))
    elif args.mode == 'run':
        env = copy(os.environ).update(env)
        command = args.command or ['bash']
        subprocess.run(command, env=env)

if __name__ == '__main__':
    main()

