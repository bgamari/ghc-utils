#!/usr/bin/env bash

pkg_name="$1"
if [ -z "$pkg_name" ]; then
    echo "usage: $0 (pkgname)"
    exit 1
fi

if [ -e "$pkg_name" ]; then
    echo "error: ./$pkg_name already exists"
    exit 1
fi

top="$(dirname "$0")/skeleton"
cp -R "$top" "$pkg_name"
mv "$pkg_name/skeleton.cabal" "$pkg_name/$pkg_name.cabal"
sed -i -e "s/\$PKGNAME/$pkg_name/" "$pkg_name/$pkg_name.cabal"
