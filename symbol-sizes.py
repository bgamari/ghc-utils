from elftools.elf.elffile import ELFFile


def print_symbols(file):
    elf_file = ELFFile(file)

    symtab = elf_file.get_section_by_name('.symtab')
    if symtab is None:
        print("No symbol table found.")
        return

    symbols = [(symbol.name, symbol['st_size'])
               for symbol in symtab.iter_symbols()]
    symbols.sort(key=lambda x: x[1], reverse=True)

    for symbol, size in symbols:
        print("{:<10} {:<40}".format(size, symbol))


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=argparse.FileType('rb'))
    args = parser.parse_args()

    print_symbols(args.file)
