#!/usr/bin/env python3

import subprocess
import textwrap
from pathlib import Path

def write_wrapper(path: Path, s: str):
    path.write_text(textwrap.dedent(s).strip())
    path.chmod(0o775)

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('ghc', type=Path, help='path to ghc executable')
    parser.add_argument("-o", "--output", type=Path, default=Path('wrapped-ghc'), help="output directory")
    parser.add_argument("--ghc-args", action='append', help="options to add to GHC command-line")
    parser.add_argument("--rts-args", action='append', help="RTS options to add to GHC command-line")
    parser.add_argument("--wrapper", help="command to wrap GHC invocation in (e.g. rr)")
    parser.add_argument("--log", type=Path, help="file in which to log invocations")
    args = parser.parse_args()

    ghc = args.ghc.resolve()
    subprocess.check_call([ghc, '--version'])

    out = args.output
    out.mkdir(parents=True, exist_ok=True)
    ghc_args = args.ghc_args or []
    if args.rts_args:
        ghc_args += ['+RTS'] + args.rts_args + ['-RTS']

    wrapper = args.wrapper or ""
    if args.log:
        log_path = args.log.resolve()
        write_wrapper(out / 'ghc', f'''
            #!/bin/sh
            echo "$(date)($BASHPID): {ghc} $@ {" ".join(ghc_args)}" >> {log_path}
            {wrapper} "{ghc}" $@ {" ".join(ghc_args)}
            echo "$(date)($BASHPID): $?" >> {log_path}
            exit $?
        ''')
    else:
        write_wrapper(out / 'ghc', f'''
            #!/bin/sh
            exec {wrapper} "{ghc}" $@ {" ".join(ghc_args)}
        ''')

    def simple_wrap(prog: str):
        path = ghc.parent / prog
        write_wrapper(out / prog, f'''
            #!/bin/sh
            exec "{path}" $@
        ''')

    simple_wrap('ghc-pkg')
    simple_wrap('haddock')
    simple_wrap('hsc2hs')

    print(out.resolve() / 'ghc')

if __name__ == '__main__':
    main()
