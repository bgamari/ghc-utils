#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A small hack to sample process statistics from procfs.

Typically used with the VmRSS and RssAnon metrics.
"""

from pathlib import Path
import sys
import time
import json
import subprocess
from typing import List, Optional, Iterator

def read_proc(pid: int) -> dict:
    f = Path('/proc') / str(pid) / 'status'
    result = {}
    for line in open(f).readlines():
        k,v = line.split(':')
        result[k.strip()] = v.strip()

    return result

def plot_it(results: "numpy.ndarray",
            out_file: Optional[Path],
            labels: List[str]):
    import matplotlib
    if out_file is not None:
        matplotlib.use('Agg')

    from matplotlib import pyplot as pl
    import numpy as np
    cols = results.shape[1]
    t = results[:,0] - results[0,0]
    for col, label in zip(range(1, cols), labels):
        pl.subplot(cols-1,1,col)
        y = results[:,col]
        pl.scatter(t, y)
        pl.ylim(0, y.max()*1.1)
        pl.ylabel(label)

    pl.xlabel('time (seconds)')
    if out_file:
        pl.savefig(out_file)
    else:
        pl.show()

def parse_smaps(s: str) -> Iterator[dict]:
    cur = {}
    for l in s.split('\n'):
        parts = l.split()
        if len(parts) == 0:
            yield cur # end of file
        if parts[0].endswith(':'):
            cur[parts[0][:-1]] = ' '.join(parts[1:])
        else:
            if cur != {}:
                yield cur

            start_s, end_s = parts[0].split('-')
            perms = parts[1]
            cur = {
                'start': int(start_s, 16),
                'end': int(end_s, 16),
                'perms': parts[1],
                'offset': int(parts[2], 16),
                'dev': parts[3],
                'inode': int(parts[4], 16),
                'pathname': parts[5] if len(parts) >= 6 else '',
            }

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--metric', action='append', default=[], type=str, help='metric names')
    parser.add_argument('-s', '--period', type=float, help='sampling period (seconds)', default=0.1)
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), help='output file', default=sys.stdout)
    parser.add_argument('-P', '--plot', nargs='?', help='plot it', default=False, const=True)
    parser.add_argument('-p', '--pid', type=int, help='process id')
    parser.add_argument('-S', '--smaps', type=int, metavar='N', help='dump smaps to ./smaps/ every N samples')
    parser.add_argument('-j', '--json', type=argparse.FileType('w'), help='JSON output file')
    parser.add_argument('cmdline', type=str, nargs='*', help='command-line to run')
    args = parser.parse_args()

    metrics = args.metric
    period = args.period
    output = args.output
    smaps = args.smaps

    if args.pid is not None and args.cmdline != []:
        raise ValueError("Both pid and command-line given")
    elif args.pid is not None:
        pid = args.pid
    elif args.cmdline == []:
        raise ValueError("Expected either pid or (non-empty) command-line")
    else:
        subp = subprocess.Popen(args.cmdline)
        pid = subp.pid

    if args.metric == [] and not args.json:
        raise ValueError("Expected either --json or at least one --metric flag")

    if args.plot and output == sys.stdout:
        print("Must output to file in order to plot", file=sys.stderr)
        sys.exit(1)

    SMAPS_DIR = Path('smaps')
    if smaps is not None:
        SMAPS_DIR.mkdir(parents=True, exist_ok=True)

    t0 = time.time()
    json_out = args.json

    try:
        i = 0
        while True:
            t1 = time.time()
            t = t1 - t0

            result = read_proc(pid)
            values = [result[metric].split()[0] for metric in metrics]
            output.write('\t'.join([str(t)] + values) + '\n')
            output.flush()

            if json_out is not None:
                json_obj = { 'time': t, 'stat': result, 'smaps': None }

            if smaps is not None:
                if i % smaps == 0:
                    f = Path('/proc') / str(pid) / 'smaps'
                    s = f.read_text()
                    (SMAPS_DIR / f'{t}').write_text(s)

                    if json_out is not None:
                        json_obj['smaps'] = list(parse_smaps(s))

                i += 1

            if json_out is not None:
                json.dump(json_obj, json_out)
                json_out.write('\n')

            time.sleep(period)

    except Exception as e:
        import traceback
        traceback.print_exc(file=sys.stderr)

    if args.plot is not False:
        if args.plot is True:
            out_file = None
        else:
            out_file = args.plot

        import numpy as np
        results = np.genfromtxt(args.output.name)
        plot_it(results, out_file, labels=metrics)

if __name__ == '__main__':
    main()
