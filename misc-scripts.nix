let sources = import nix/sources.nix;
in
{ pkgs ? (import sources.nixpkgs {}) }:

with pkgs;
stdenv.mkDerivation {
  name = "misc-scripts";
  nativeBuildInputs = [ makeWrapper ];
  preferLocalBuild = true;
  buildCommand = ''
    mkdir -p $out/bin

    makeWrapper ${./wrap_ghc.py} $out/bin/wrap-ghc \
      --prefix PATH : ${python3}/bin
    makeWrapper ${./add-upstream-remotes.py} $out/bin/add-upstream-remotes
    makeWrapper ${./review-submodules} $out/bin/review-submodules
    makeWrapper ${./split-core2core.py} $out/bin/split-core2core
    makeWrapper ${./eventlog-sort.sh} $out/bin/eventlog-sort \
      --prefix PATH : ${haskellPackages.ghc-events}/bin:${gawk}/bin
    makeWrapper ${./run-until-crash} $out/bin/run-until-crash \
      --prefix PATH : ${python3}/bin
    makeWrapper ${./make-ghc-tags.sh} $out/bin/make-ghc-tags \
      --prefix PATH : ${haskellPackages.fast-tags}/bin
    makeWrapper ${./diff-submodules.py} $out/bin/diff-submodules \
      --prefix PATH : ${python3}/bin
    makeWrapper ${./build-cabal.sh} $out/bin/build-cabal

    makeWrapper ${./rts_stats.py} $out/bin/rts-stats
    makeWrapper ${./ghc_perf.py} $out/bin/ghc-perf
    makeWrapper ${./perf-compare.py} $out/bin/ghc-perf-compare \
      --prefix PATH : ${python3.withPackages (pkgs: with pkgs; [ matplotlib pandas tabulate ])}/bin
    makeWrapper ${./sample_proc.py} $out/bin/sample-proc \
      --prefix PATH : ${python3.withPackages (pkgs: with pkgs; [ matplotlib pandas tabulate ])}/bin
    makeWrapper ${./ghc_timings.py} $out/bin/ghc-timings \
      --prefix PATH : ${python3.withPackages (pkgs: with pkgs; [ matplotlib pandas tabulate ])}/bin
    makeWrapper ${./rel-eventlog} $out/bin/rel-eventlog \
      --prefix PATH : ${pkgs.gawk}/bin
  '';
}

