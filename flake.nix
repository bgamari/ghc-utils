{
  description = "ghc-utils";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages = rec {
          misc-scripts = import ./misc-scripts.nix { inherit pkgs; };
          ghc-gdb = import ./gdb { inherit pkgs; };
          rel-eng = import ./rel-eng { inherit pkgs; };
          gitlab-utils = import ./gitlab-utils { inherit pkgs; };
          compare-ticks = pkgs.haskellPackages.callCabal2nix "compare-ticks" ./compare-ticks {};
          filter-dot = pkgs.haskellPackages.callCabal2nix "filter-dot" ./filter-dot {
            algebraic-graphs = pkgs.haskell.lib.dontCheck (pkgs.haskellPackages.callHackage "algebraic-graphs" "0.7" {});
          };
          distro-versions = pkgs.haskellPackages.callCabal2nix "distro-versions" ./rel-eng/distro-versions {};
          library-versions = pkgs.haskellPackages.callCabal2nix "library-versions" ./library-versions {};
          symbol-sizes = pkgs.writers.writePython3Bin "symbol-sizes" {
            libraries = [ pkgs.python3Packages.pyelftools ];
          } (builtins.readFile ./symbol-sizes.py);
          all =
            pkgs.symlinkJoin {
              name = "ghc-utils";
              preferLocalBuild = true;
              paths = [
                ghc-gdb.debug-ghc ghc-gdb.rr ghc-gdb.gdb ghc-gdb.run-ghc-gdb ghc-gdb.run-ghc-rr ghc-gdb.dot2svg ghc-gdb.zen-workaround
                misc-scripts
                rel-eng
                gitlab-utils
                compare-ticks
                filter-dot
              ];
            };
          default = all;
        };

        apps = rec {
          compare-ticks = flake-utils.lib.mkApp { drv = self.packages.${system}.compare-ticks; };
          ghc-gdb = flake-utils.lib.mkApp {
            drv = self.packages.${system}.ghc-gdb.run-ghc-gdb;
            exePath = "/bin/ghc-gdb";
          };
          ghc-rr = flake-utils.lib.mkApp {
            drv = self.packages.${system}.ghc-gdb.run-ghc-rr;
            exePath = "/bin/ghc-rr";
          };
          zen-workaround = flake-utils.lib.mkApp {
            drv = self.packages.${system}.ghc-gdb.zen-workaround;
            exePath = "/bin/zen_workaround.py";
          };
          dot2svg = flake-utils.lib.mkApp {
            drv = self.packages.${system}.ghc-gdb.dot2svg;
          };
          distro-versions = flake-utils.lib.mkApp {
            drv = self.packages.${system}.distro-versions;
          };
          make-library-table = flake-utils.lib.mkApp {
            drv = self.packages.${system}.library-versions;
            exePath = "/bin/make-version-table";
          };
          threadscope = flake-utils.lib.mkApp {
            drv = pkgs.haskellPackages.threadscope.bin;
            exePath = "/bin/threadscope";
          };
          split-core2core = flake-utils.lib.mkApp {
            drv = self.packages.${system}.misc-scripts;
            exePath = "/bin/split-core2core";
          };
          ghc-events = flake-utils.lib.mkApp {
            drv = pkgs.haskellPackages.ghc-events;
            exePath = "/bin/ghc-events";
          };
          rel-eventlog = flake-utils.lib.mkApp {
            drv = self.packages.${system}.misc-scripts;
            exePath = "/bin/rel-eventlog";
          };
        };
      }
    );
}
