The scripts contained within make use of the gitlab API which requires configuration.

The simplest way to do this currently is:

* Get an API token for our gitlab instance:
  Gitlab -> User icon -> Preferences -> Access Tokens -> Add token

* Create a config: `vim ~/.python-gitlab.cfg` looking like this:

```
[global]
default = haskell
ssl_verify = true
timeout = 5

[haskell]
url = https://gitlab.haskell.org/
private_token = ELIDED<your token here>
ssl_verify = True
api_version = 4
```
