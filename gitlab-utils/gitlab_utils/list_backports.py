#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import gitlab
from gitlab.v4.objects import Project, ProjectMergeRequest
import click
from datetime import datetime, timezone
from typing import List, Optional

from util import find_merge_commits


@click.command()
@click.option('-l', '--label', required=True, type=str, help='GitLab label (e.g. "backport needed:9.8")')
def cli(label: str):
    gl = gitlab.Gitlab.from_config('haskell')
    proj = gl.projects.get(1)

    sort_key = lambda x: datetime.fromisoformat(x.closed_at) if x.closed_at else datetime.now(tz=timezone.utc)

    for mr in sorted(proj.mergerequests.list(lazy=True, labels=[label], order_by='updated_at', sort='asc', all=True), key=sort_key):
        do_mr(proj, mr)


def do_mr(project: Project, mr: gitlab.v4.objects.ProjectMergeRequest):
    master_commits = find_merge_commits(project, mr)

    # MR commits couldn't be found on master branch yet.
    if not master_commits:
        print(f' * [ ] !{mr.iid}: {mr.title} ("not-merged")')
        return

    mr_commits = list(mr.commits())
    n_commits = len(mr_commits)
    if n_commits > 1:
        # Check the last n commits on master match the n commits on the mr:
        for i in range(0, n_commits):
            if(mr_commits[i].title != master_commits[i].title):
                # print(mr_commits[i].title, master_commits[i].title)
                print(f' * [ ] !{mr.iid}: {mr.title} (not-all-found)')
                return

    # Give the commits in the order in which we want to cherry pick them
    commits_str = " ".join(map(lambda c: c.id, reversed(master_commits))) if master_commits else "not-merged"

    print(f' * [ ] !{mr.iid}: {mr.title} ({commits_str})')


if __name__ == '__main__':
    cli()


