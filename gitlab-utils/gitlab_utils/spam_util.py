#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import timedelta, datetime, timezone
from dateutil.parser import isoparse
import click
import gitlab
from typing import List, Dict, Iterator
import io
import logging

logging.basicConfig(level=logging.INFO)

UserName = str

USER_WHITELIST = '''
bgamari
ngamari
marge-bot
mpickering
AndreasK
sgraf812
simonpj
RyanGlScott
wz1000
int-index
abrar
complyue
theSeafarer
dcoutts
kosmikus
Cale
osa1
carter
simonmar
nineonine
chreekat
alp
maerwald
duog
adinapoli
Kleidukos
emilypi
Bodigrim
ged2102
'''.split()

@click.group()
def cli():
    pass

@cli.command()
@click.option('-o', '--output', type=click.File('w'))
def dump_snippets(output) -> None:
    gl = gitlab.Gitlab.from_config()
    with click.progressbar(gl.snippets.public(iterator=True, all=True, lazy=True), label='Listing spam snippets') as snippets:
        for x in snippets:
            u = gl.users.get(x.author['id'])
            if not good_user(gl, u):
                output.write('%-20s    # (!%d) %s\n' % (u.username, x.id, x.title))

def parse_user_list(input) -> Iterator[UserName]:
    for l in input.read().split('\n'):
        if '#' in l:
            l = l.split('#')[0]

        l = l.strip()
        yield l

@cli.command()
@click.option('-o', '--output', type=click.File('w'))
def list_bad_users(output) -> None:
    gl = gitlab.Gitlab.from_config()
    with click.progressbar(gl.users.list(iterator=True, all=True, lazy=True), label='Listing users') as users:
        for user in users:
            try:
                if user.username in USER_WHITELIST:
                    continue

                if user.email.find('zopesystems.com') != -1:
                    output.write(f"{user.id}    # {user.username}\n")
            except Exception as e:
                logging.warning(f"error while processing {user}: {e}")

@cli.command()
@click.option('-o', '--output', type=click.File('w'))
def find_bad_projects(output) -> None:
    VIETNAMESE_WORDS = {
        '-phuc-', '-tai-', '-app-', 'thuyet-', '-ly-', 'huong'
    }
    gl = gitlab.Gitlab.from_config()
    with click.progressbar(gl.projects.list(iterator=True, all=True, lazy=True), label='Listing projects') as projects:
        for proj in projects:
            try:
                if proj.owner['username'] in USER_WHITELIST:
                    continue

                if proj.path_with_namespace.find('ghc') != -1 or proj.path_with_namespace.find('haskell') != -1:
                    continue

                files = proj.repository_tree()
                if len(files) == 1 and files[0]['name'] == 'README.md':
                    output.write(f"{proj.owner['username']}    # {proj.path_with_namespace}\n")
            except Exception as e:
                logging.warning(f"error while processing {proj}: {e}")

def good_user(gl, u) -> bool:
    if u.username in USER_WHITELIST:
        logging.info(f"Skipping {u.username} (whitelist).")
        return True

    if u.is_admin:
        logging.info(f"Skipping {u.username} (administrator).")
        return True

    issues = gl.issues.list(author_id=u.id)
    if len(issues) != 0:
        logging.info(f"Skipping {u.username} (found {len(issues)} issues).")
        return True

    mrs = gl.mergerequests.list(author_id=u.id)
    if len(mrs) != 0:
        logging.info(f"Skipping {u.username} (found {len(mrs)} MRs).")
        return True

    return False

@cli.command()
@click.option('--dryrun', is_flag=True)
def reject_pending_requests(dryrun: bool):
    gl = gitlab.Gitlab.from_config()
    with click.progressbar(gl.users.list(iterator=True, all=True, lazy=True, without_projects=True), label='Listing users') as users:
        for u in users:
            if u.state == 'blocked_pending_approval':
                logging.info(f'Rejecting approval request for {u.username}')
                if not dryrun:
                    u.reject()

@cli.command()
@click.argument('input', type=click.File('r'))
@click.option('--limit', type=int)
@click.option('--block', is_flag=True)
@click.option('--delete', is_flag=True)
def delete_users(input, limit: int, block: bool, delete: bool):
    if block and delete:
        raise click.UsageError('Only one of --block/--delete may be used')

    users = set(parse_user_list(input))
    print(f'{len(users)} users')
    print('\n'.join(f'  {u}' for u in users))
    click.confirm(f'Delete {len(users)} users?', abort=True)

    deleted = 0
    gl = gitlab.Gitlab.from_config()
    with click.progressbar(users) as bar:
        for username in bar:
            us = gl.users.list(username=username)
            if len(us) != 1:
                logging.info(f"Skipping {username} (found {len(us)} matching users).")
                continue

            u = us[0]
            if good_user(gl, u):
                logging.info(f'{u.username} is good, not deleting')
                continue

            deleted += 1
            if block:
                logging.info(f'deactivating {u.username}')
                u.block()
            elif delete:
                logging.info(f'deleting {u.username}')
                u.delete(hard_delete=True)
            else:
                logging.info(f'would delete {u.username}')

            if limit is not None and deleted >= limit:
                return

    logging.info(f'deleted {deleted} users.')

def inactive_user(gl, u, thresh: timedelta = timedelta(days=180)) -> bool:
    if u.last_sign_in_at is None:
        return True

    last = isoparse(u.last_sign_in_at)
    return (datetime.now(timezone.utc) - last) > thresh

@cli.command()
@click.option('-o', '--output', type=click.File('w'))
def list_inactive_users(output) -> None:
    gl = gitlab.Gitlab.from_config()
    with click.progressbar(gl.users.list(iterator=True, all=True, lazy=True), label='Listing inactive users') as users:
        for u in users:
            if inactive_user(gl, u) and not good_user(gl, u):
                output.write('%-20s    # %s\n' % (u.username, u.last_sign_in_at))
