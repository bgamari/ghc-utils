# -*- coding: utf-8 -*-

import re
import gitlab
from gitlab.v4.objects import Project, ProjectMergeRequest, ProjectCommit
from typing import List, Optional

# Find the sha of the commit marge reported in the discussion
def find_marge_commit(mr: gitlab.v4.objects.ProjectMergeRequest) -> Optional[str]:
    for note in mr.notes.list(lazy=True, all=True):
        if note.author['username'] != 'marge-bot':
            continue

        m = re.match(r'Merged in ([0-9a-f]*)', note.body, re.IGNORECASE)
        if m:
            return m.group(1)

    return None

# Find all the merged commits on master
def find_merge_commits(project: Project, mr: ProjectMergeRequest) -> Optional[List[ProjectCommit]]:
    n_commits = len(mr.commits())
    sha = find_marge_commit(mr)
    if not sha:
        return None

    commits = []
    for i in range(n_commits):
        commit = project.commits.get(sha)
        commits += [commit]
        if len(commit.parent_ids) != 1:
            return None

        sha = commit.parent_ids[0]

    return list(commits)