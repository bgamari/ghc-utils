let sources = import nix/sources.nix;
in
{ pkgs ? (import sources.nixpkgs {}) }:

let
  misc-scripts = import ./misc-scripts.nix { inherit pkgs; };
  gdb = import ./gdb;
  gitlab-utils = import ./gitlab-utils { inherit pkgs; };
  rel-eng = import ./rel-eng { inherit pkgs; };
  inherit (pkgs) haskellPackages;
  compare-ticks = haskellPackages.callCabal2nix "compare-ticks" ./compare-ticks {};
  filter-dot = haskellPackages.callCabal2nix "filter-dot" ./filter-dot {
    algebraic-graphs = pkgs.haskell.lib.dontCheck (haskellPackages.callHackage "algebraic-graphs" "0.7" {});
  };
in
  pkgs.symlinkJoin {
    name = "ghc-utils";
    preferLocalBuild = true;
    paths = [
      gdb.debug-ghc gdb.rr gdb.gdb gdb.run-ghc-gdb gdb.run-ghc-rr gdb.dot2svg gdb.zenWorkaround
      misc-scripts
      rel-eng
      gitlab-utils
      compare-ticks
      filter-dot
    ];
  }
