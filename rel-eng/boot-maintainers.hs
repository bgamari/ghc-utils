{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use camelCase" #-}
import Data.List (intercalate)
import qualified Data.Set as S

type BootPackage = String
type Email = String

hecate = "Hécate <hecate@glitchbra.in>"
mikolaj = "Mikolaj Konarski <mikolaj@well-typed.com>"
tamar = "Tamar Christina <tamar@zhox.com>"
hasufell = "Julian Ospald <hasufell@hasufell.de>"
bodigrim = "Andrew Lelechenko <andrew.lelechenko@gmail.com>"
judah = "Judah Jacobson <judah.jacobson@gmail.com>"
david_binder = "David Binder <david.binder@uni-tuebingen.de>"
tom_ellis = "Tom Ellis <tom-2021@tomellis.org>"

bootMaintainers :: [(BootPackage, Email)]
bootMaintainers =
    [ "text"         --> bodigrim
    , "filepath"     --> hasufell
    , "file-io"      --> hasufell
    , "deepseq"      --> "Melanie Phoenix <brown.m@proton.me>"
    , "containers"   --> "David Feuer <david.feuer@gmail.com>"
    , "Cabal"        --> mikolaj
    , "Cabal"        --> hecate
    , "Cabal"        --> "Brandon S. Allbery <allbery.b@gmail.com>"
    , "win32"        --> tamar
    , "directory"    --> "Phil Rufflewind <rf@rufflewind.com>"
    , "binary"       --> "Lennart Kolmodin <kolmodin@gmail.com>"
    , "exceptions"   --> "Ryan Scott <rscott@galois.com>"
    , "terminfo"     --> judah
    , "haskeline"    --> judah
    , "hpc"          --> david_binder
    , "hpc-bin"      --> david_binder
    , "mtl"          --> "Emily Pillmore <emilypi@cohomolo.gy>"
    , "parsec"       --> "Oleg Grenrus <oleg.grenrus@iki.fi>"
    , "time"         --> "Ashley Yakeley <ashley@semantic.org>"
    , "stm"          --> "Simon Marlow <smarlow@fb.com>"
    , "process"      --> "Michael Snoyman <michael@snoyman.com>"
    , "process"      --> "Ben Gamari <ben@well-typed.com>"
    , "process"      --> tom_ellis
    , "transformers" --> "Ross Paterson <R.Paterson@city.ac.uk>"
    , "Cabal"        --> "Hécate <hecate@glitchbra.in>"
    , "containers"   --> "Soumik Sarkar <soumiksarkar.3120@gmail.com>"
    , "array"        --> "Lei Zhu <julytreee@gmail.com>"
    , "array"        --> "Miao ZhiCheng <miao@decentral.ee>"
    , "array"        --> "Carsten König <Carsten.Koenig@hotmail.de>"
    , "bytestring"   --> "Matthew Craven <clyring@gmail.com>"
    , "text"         --> "Li-yao Xia <lysxia@gmail.com>"
    ]
  where (-->) = (,)

releaseList :: S.Set Email
releaseList = S.fromList
    [ "haskell@haskell.org"
    , "ghc-devs@haskell.org"
    , "glasgow-haskell-users@haskell.org"
    , "haskell-cafe@googlegroups.com"
    , "ghc-releases@haskell.org"
    ]

emailList :: S.Set Email -> String
emailList = intercalate ", " . S.toList

bootMaintainerEmails :: String
bootMaintainerEmails =
    emailList $ S.fromList $ map snd bootMaintainers

main :: IO ()
main = do
    putStrLn "Boot maintainer list:"
    putStrLn bootMaintainerEmails
