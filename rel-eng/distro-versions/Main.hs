{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import qualified Data.Text as T
import Data.Version
import Data.Monoid
import Data.Monoid
import qualified Data.Map.Strict as M
import qualified Data.Set as S

import Text.Tabular
import Text.Tabular.Html
import Text.Html (stringToHtml)
import Servant.Client (runClientM)

import Repology

data LibcImpl = Glibc | Musl
              deriving (Eq, Ord, Show)

data Distro = Distro { distroName :: Text
                     , distroRepo :: RepoName
                     , distroSubRepo :: SubRepoName
                     , distroLibc :: LibcImpl
                     }
            deriving (Eq, Ord, Show)

relevantProjects :: [ProjectName]
relevantProjects =
  map ProjectName
  [ "gcc"
  , "binutils"
  , "glibc"
  , "gmp"
  , "ncurses"
  , "tinfo"
  ]

linkedProjects :: S.Set ProjectName
linkedProjects =
  S.fromList $ map ProjectName
  [ "glibc"
  , "gmp"
  , "ncurses"
  ]

distros :: [Distro]
distros =
  [ fedora "27"
  , fedora "28"
  , fedora "29"
  , fedora "30"
  , fedora "31"
  , fedora "32"
  , fedora "33"
  , fedora "34"
  , fedora "35"
  , fedora "36"
  , fedora "37"
  , fedora "38"
  , fedora "39"
  , fedora "40"
  , fedora "41"

  , centos "6"
  , centos "7"
  , centos "8"
  , centos_stream "9"

  , ubuntu "16.04" "xenial"
  , ubuntu "18.04" "bionic"
  , ubuntu "19.10" "eoan"
  , ubuntu "20.04" "focal"
  , ubuntu "22.04" "jammy"
  , ubuntu "22.10" "kinetic"
  , ubuntu "23.04" "lunar"
  , ubuntu "23.10" "mantic"
  , ubuntu "24.04" "noble"
  , ubuntu "24.10" "oracular"

  , debian "8"  "jessie"
  , debian "9"  "stretch"
  , debian "10" "buster"
  , debian "11" "bullseye"
  , debian "12" "bookworm"
  , debian "13" "trixie"

  , alpine "3_8"
  , alpine "3_9"
  , alpine "3_10"
  , alpine "3_11"
  , alpine "3_12"
  , alpine "3_13"
  , alpine "3_14"
  , alpine "3_15"
  , alpine "3_16"
  , alpine "3_17"
  , alpine "3_18"
  , alpine "3_19"
  , alpine "3_20"
  , alpine "3_21"
  --, freebsd "11"

  , rocky "8"
  , rocky "9"
  ]
  where
    rocky ver = 
      Distro { distroName = "Rocky " <> ver
             , distroRepo = RepoName $ "rocky_" <> ver
             , distroSubRepo = SubRepoName "BaseOS"
             , distroLibc = Glibc
             }
    mint ver =
      Distro { distroName = "Mint " <> ver
             , distroRepo = RepoName $ "linux_mint_" <> ver
             , distroSubRepo = SubRepoName "release"
             , distroLibc = Glibc
             }
    fedora ver =
      Distro { distroName = "Fedora " <> ver
             , distroRepo = RepoName $ "fedora_" <> ver
             , distroSubRepo = SubRepoName "release"
             , distroLibc = Glibc
             }
    centos ver =
      Distro { distroName = "CentOS " <> ver
             , distroRepo = RepoName $ "centos_" <> ver
             , distroSubRepo = SubRepoName "os"
             , distroLibc = Glibc
             }
    centos_stream ver =
      Distro { distroName = "CentOS Stream " <> ver
             , distroRepo = RepoName $ "centos_stream_" <> ver
             , distroSubRepo = SubRepoName "os"
             , distroLibc = Glibc
             }
    ubuntu ver code =
      Distro { distroName = "Ubuntu " <> ver <> " (" <> code <> ")"
             , distroRepo = RepoName $ "ubuntu_" <> T.map f ver
             , distroSubRepo = SubRepoName $ code <> "/main"
             , distroLibc = Glibc
             }
      where f '.' = '_'
            f x   = x
    debian ver code =
      Distro { distroName = "Debian " <> ver
             , distroRepo = RepoName $ "debian_" <> ver
             , distroSubRepo = SubRepoName $ code <> "/main"
             , distroLibc = Glibc
             }
    alpine ver =
      Distro { distroName = "Alpine " <> ver
             , distroRepo = RepoName $ "alpine_" <> ver
             , distroSubRepo = SubRepoName "main"
             , distroLibc = Musl
             }

truncateVersion :: Int -> Version -> Version
truncateVersion n (Version xs _) = Version (take n xs) []
--truncateVersion n = id

okayPackage :: Package -> Bool
okayPackage pkg
  | "compat-gcc" `T.isPrefixOf` getPackageName (pkgName pkg) = False
  | otherwise = True

fetchProjects :: [ProjectName] -> IO (M.Map ProjectName [Package])
fetchProjects projs = do
  clientEnv <- defaultClientEnv
  runClientM (mapM getProject $ M.fromList [(p,p) | p <- projs]) clientEnv >>= either (fail . show) return

main :: IO ()
main = do
  projectPackages <- fetchProjects relevantProjects
  --mapM_ (mapM_ print) projectPackages

  let repoToDistro :: M.Map (RepoName, SubRepoName) Distro
      repoToDistro =
        M.fromList
        [ ((distroRepo d, distroSubRepo d), d)
        | d <- distros
        ]

  let distroPackages :: M.Map Distro (M.Map ProjectName Version)
      distroPackages =
        M.fromListWith (<>)
        [ (distro, M.singleton projName v)
        | (projName, pkgs) <- M.toList projectPackages
        , pkg <- fmap (\pkg -> pkg { pkgVersion = fmap (truncateVersion 2) $ pkgVersion pkg }) pkgs
        , okayPackage pkg
        , Just distro <- pure $ M.lookup (pkgRepo pkg, pkgSubRepo pkg) repoToDistro
        , Right v <- pure $ pkgVersion pkg
        ]
  let packagesTable :: Table Distro ProjectName (Maybe Version)
      packagesTable = foldl (+.+) table0
        [ row distro [ M.lookup proj packages | proj <- relevantProjects ]
        | (distro, packages) <- M.toList distroPackages
        ]
      table0 :: Table Distro ProjectName (Maybe Version)
      table0 = Table (Group NoLine []) (Group NoLine $ map Header relevantProjects) []
  let render = Text.Tabular.Html.render
                 (stringToHtml . T.unpack . distroName)
                 (stringToHtml . T.unpack . getProjectName)
                 (stringToHtml . maybe "?" showVersion)
  writeFile "versions.html" $ show $ render packagesTable

  let combinations :: M.Map (M.Map ProjectName Version) (S.Set Distro)
      combinations =
        M.fromListWith (<>)
        [ (x `M.restrictKeys` linkedProjects, S.singleton y)
        | (y, x) <- M.toList distroPackages
        ]
  mapM_ putStrLn $ 
    [ unwords [ T.unpack proj <> "-" <> showVersion ver
              | (ProjectName proj, ver) <- M.toList pkgs
              ]
      <> "\t" <> show (fmap distroName $ S.toList distros)
    | (pkgs, distros) <- M.toList combinations
    ]

