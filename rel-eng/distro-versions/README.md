This program starts from a hard-coded list of interesting distros and finds all
unique ABIs that are generated from it.

By ABI, we mean the set of major version of important system-level packages,
like gcc and ncurses.

On stdout, you get the list of unique ABIs and the distros that map to them.

```
glibc-2.12 gmp-4.3 ncurses-5.7  ["CentOS 6"]
glibc-2.17 gmp-6.0 ncurses-5.9  ["CentOS 7"]
glibc-2.19 gmp-6.1 ncurses-6.0  ["Ubuntu 16.04 (xenial)"]
glibc-2.26 gmp-6.1 ncurses-6.0  ["Fedora 27"]
glibc-2.27 gmp-6.1 ncurses-6.1  ["Fedora 28","Ubuntu 18.04 (bionic)"]
glibc-2.28 gmp-6.1 ncurses-6.1  ["Debian 10","Fedora 29","Rocky 8"]
glibc-2.29 gmp-6.1 ncurses-6.1  ["Fedora 30"]

<snip>
```

A full table of results is also written to a file `versions.html`. In textual
form, the table looks like

```
                gcc     binutils    glibc   gmp     ncurses     tinfo
Alpine 3_10     8.3     2.32        ?       6.1     6.1         ?
Alpine 3_11     9.3     2.33        ?       6.1     5.9         ?
Alpine 3_12     9.3     2.34        ?       6.2     6.2         ?
Alpine 3_13     10.2    2.35        ?       6.2     6.2         ?
Alpine 3_14     10.3    2.35        ?       6.2     6.2         ? 

<snip>
```
