# Setting up FreeBSD VM for GHC development

Starting with FreeBSD VM image.
```
$ pkg install bash wget vim autoconf automake git gtar pxz devel/gmake gmp \
    ghc hs-cabal-install
$ pkg install python3 py311-sphinx texlive-full dejavu
$ # Create user, setup networking
$ bsdconfig
```
Enable the `sshd_enable`.

Build happy and alex
```
$ cabal update
$ cabal install happy alex
$ echo 'PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
$ source ~/.bashrc
```
Now checkout tree
```
$ git clone https://gitlab.haskell.org/ghc/ghc
$ cd ghc
$ git submodule update --init
$ ./boot
$ ./configure --with-system-libffi
$ hadrian/build -j5
```

## Avahi

Avahi can be useful for host discoverability:

```
$ pkg install avahi
```
Add enable the following in `/etc/rc.conf`,
```
dbus_enable="YES"
avahi_daemon_enable="YES"
```
