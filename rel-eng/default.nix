let sources = import ../nix/sources.nix; in

{ pkgs ? (import sources.nixpkgs {}) }:

with pkgs;
let
  distro-versions = pkgs.haskellPackages.callCabal2nix "distro-versions" ./distro-versions {};

  bindistPrepEnv = pkgs.buildFHSUserEnv {
    name = "enter-fhs";
    targetPkgs = pkgs: with pkgs; [
      # all
      gcc binutils gnumake gmp ncurses5 git elfutils
      # source-release.sh
      xorg.lndir curl python3 which automake autoconf m4 file
      haskell.compiler.ghc8107 haskellPackages.happy haskellPackages.alex
    ];
    runScript = "$SHELL -x";
  };

  scripts = stdenv.mkDerivation {
    name = "rel-eng-scripts";
    nativeBuildInputs = [ makeWrapper ];
    preferLocalBuild = true;
    buildCommand = ''
      mkdir -p $out/bin

      makeWrapper ${./bin-release.sh} $out/bin/bin-release.sh

      cat > $out/bin/source-release.sh <<EOF
      #!${bash}/bin/bash
      ${bindistPrepEnv}/bin/enter-fhs ${bash}/bin/bash -x ${./source-release.sh}
      EOF
      chmod ugo+rx $out/bin/source-release.sh
    '';
  };

in
  symlinkJoin {
    name = "ghc-rel-eng";
    preferLocalBuild = true;
    paths = [
      scripts
      distro-versions
    ];
  }
