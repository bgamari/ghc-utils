#!/usr/bin/env bash

set -e -x
out=_cabal_out

if [ -n "$CABAL_DIR" ]; then
    dir="$CABAL_DIR"
else
    dir="libraries/Cabal"
fi

if [[ -z "$GHC" ]]; then
  GHC="_build/stage1/bin/ghc"
fi

if [[ -z "$WRAPPER" ]]; then
  WRAPPER="time"
fi

rm -Rf $out
export GHC_ENVIRONMENT="-"
exec $WRAPPER $GHC \
    -hide-all-packages \
    -package array \
    -package base \
    -package binary \
    -package bytestring \
    -package containers \
    -package deepseq \
    -package directory \
    -package filepath \
    -package mtl \
    -package parsec \
    -package pretty \
    -package process \
    -package text \
    -package time \
    -package transformers \
    -package unix \
    -XHaskell2010 \
    -hidir $out \
    -odir $out \
    -i$dir/Cabal \
    -i$dir/Cabal-syntax/src \
    -i$dir/Cabal/src \
    -i_build/stage1/libraries/Cabal/Cabal-syntax/build \
    $dir/Cabal/Setup.hs \
    +RTS -s -RTS \
    $@
