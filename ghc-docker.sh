#!/usr/bin/env bash

set -e

usage() {
  echo "$0 (image url)"
  exit 1
}

if [[ -z "$BUILD_FLAVOUR" ]]; then
  BUILD_FLAVOUR="validate"
fi

if [[ -z "$IMAGE" ]]; then
  IMAGE="$1"
  shift
fi
if [[ -z "$IMAGE" ]]; then
  usage
fi

COMMIT="$2"
if [[ -z "$COMMIT" ]]; then
  COMMIT="master"
fi

#C=$(docker container create --name="ghc-docker" $IMAGE)

tmp="$(mktemp -d)"
mkdir -p "$tmp"
cat >"$tmp/Dockerfile" <<EOF
FROM $IMAGE
RUN git config --global user.email "$(git config --global --get user.email)" && \
    git config --global user.name "$(git config --global --get user.name)"
RUN if which apt-get; then \
      sudo apt-get update && sudo apt-get install -y vim htop tmux gdb strace; \
    elif which apk; then \
      sudo apk add vim htop tmux gdb strace; \
    elif which dnf; then \
      sudo dnf install -y vim htop tmux gdb strace; \
    fi
RUN git clone https://gitlab.haskell.org/bgamari/ghc-utils
RUN git clone https://gitlab.haskell.org/ghc/ghc && \
    git -C ghc checkout $COMMIT && \
    git -C ghc submodule update --init
RUN cd ghc && \
    .gitlab/ci.sh setup && \
    .gitlab/ci.sh configure
WORKDIR /home/ghc/ghc
ENV BIN_DIST_NAME tmp
ENV BUILD_FLAVOUR $BUILD_FLAVOUR
CMD bash -c 'while true; do sleep 60; done;'
EOF

iidfile="$(mktemp)"
rm $iidfile
docker build --iidfile="$iidfile" "$tmp"
image="$(cat "$iidfile")"
rm -f "$iidfile"
#rm -R "$tmp"
echo "Development environment image is:"
echo "  $image"

cidfile="$(mktemp -u)"
docker create --cidfile="$cidfile" "$image"
container="$(cat "$cidfile")"
rm -f "$cidfile"
echo
echo "Development environment container is:"
echo "  $container"
echo
echo "To start another shell in container run:"
echo
echo "    docker exec -it $container bash -i"
echo

docker start "$container"
docker exec -it "$container" bash -i
echo
echo "To drop container run:"
echo
echo "    docker container rm --force --volumes $container"
echo
