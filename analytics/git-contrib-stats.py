#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Compute how many contributors have at least N commits for each year.
"""

from typing import List, Set, TypeVar, Dict
import subprocess
from collections import defaultdict
from datetime import datetime

import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as pl

def commit_authors_during(since: str, until: str) -> List[str]:
    out = subprocess.check_output(['git', 'log', '--format=%an', f'--since={since}', f'--until={until}'], encoding='UTF-8')
    return list(out.split('\n'))

A = TypeVar('A')

def counts(xs: List[A]) -> Dict[A, int]:
    cs = defaultdict(lambda: 0)
    for x in xs:
        cs[x] += 1

    return cs

def main() -> None:
    cur_year = datetime.now().year

    commits_by_author_by_year = {
        (cur_year-years_ago): counts(commit_authors_during(f'{cur_year-years_ago-1}-08-25', f'{cur_year-years_ago}-08-25'))
        for years_ago in range(15)
    }

    # How many authors had more than N commits?
    more_than = lambda counted, n: len(list(author for author, commit_count in counted.items() if commit_count > n))

    pl.rcParams.update({'font.size': 22})
    pl.figure(figsize=(16,9))

    out = [list(commits_by_author_by_year.keys())]
    for n in [1,2,5,10]:
        xs = [ (year, more_than(counted, n))
               for year, counted in commits_by_author_by_year.items()
             ]
        xs = np.array(xs)
        out.append(xs[:,1])
        pl.plot(xs[:,0], xs[:,1], 'o-', label=f'{n} or more commits')

    print(np.array(out, dtype='i').T)
    pl.suptitle('Number of contributors per year, grouped by commit count')
    pl.xlabel('year')
    pl.ylabel('number of contributors')
    pl.legend()
    pl.savefig('commit-counts.svg')

def pluralize(s: str, n: int):
    return s if n == 1 else f'{s}s'

if __name__ == '__main__':
    main()

