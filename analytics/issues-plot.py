#!/usr/bin/env python3

import pandas as pd
from matplotlib import pyplot as pl
from sqlalchemy import create_engine, text
import datetime

start_date = None
start_date = datetime.datetime.fromisoformat('2020-01-01')

pl.figure(figsize=(16,9))
pl.rcParams.update({'font.size': 22})

# ssh gitlab.haskell.org -L $(pwd)/.s.PGSQL.5432:/run/postgresql/.s.PGSQL.5432
CONN = 'postgresql://ben@localhost/gitlab?host=/opt/exp/ghc/ghc-utils/analytics'

engine = create_engine(CONN).connect()

def query(sql):
    return pd.read_sql(sql, engine)


##########################################################
# Number of open tickets versus time
##########################################################

q = text('''
WITH ticket_status_changes AS (
    SELECT created_at AS date, 1 AS change
    FROM issues
    WHERE project_id = 1 AND created_at IS NOT NULL

    UNION ALL

    SELECT closed_at AS date, -1 AS change
    FROM issues
    WHERE project_id = 1 AND closed_at IS NOT NULL
)
SELECT
    DATE_TRUNC('day', date) AS date,
    SUM(change) OVER (ORDER BY DATE_TRUNC('day', date)) AS cumulative_open_tickets
FROM ticket_status_changes
ORDER BY DATE_TRUNC('day', date)
''')
open_tickets = query(q)
open_tickets['date'] = open_tickets['date'].dt.tz_localize(None)
open_tickets = open_tickets.set_index(['date'])
print(open_tickets)

q = text('''
select
  date_trunc('day', created_at) as date,
  sum(1) over (order by (date_trunc('day', created_at))) as cumulative_tickets
from issues
where project_id = 1
order by date_trunc('day', created_at)
''')
all_tickets = query(q).set_index(['date'])
print(all_tickets)

stats = all_tickets.join(open_tickets)
stats = stats.groupby(['date']).last()
stats.rename(columns={'cumulative_tickets': 'All tickets', 'cumulative_open_tickets': 'Open tickets'}).plot(ax=pl.gca())
pl.suptitle('Number of open tickets versus time')
pl.xlim(start_date, None)
pl.xlabel('Date')
pl.ylabel('Number of tickets')
pl.savefig('tickets.svg')

